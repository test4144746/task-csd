#!/bin/bash
source_dir="/tmp/test/files"
log_file="/tmp/test/script-log/sync.log"
archive_dir="/tmp/test/archives"

mkdir -p "$(dirname "$log_file")"
mkdir -p "$archive_dir"

latest_file=$(find "$source_dir" -maxdepth 1 -type f -mtime -7 -printf "%T@\t%p\n" | sort -nr | head -1 | cut -f 2-)

if [ -n "$latest_file" ]; then
    file_name=$(basename "$latest_file")
    file_size=$(du -h "$latest_file" | awk '{print $1}')
    current_date=$(date "+%Y-%m-%d %H:%M:%S")
    echo "[$current_date] File: $latest_file, Name: $file_name, Size: $file_size" >> "$log_file"
    archive_name="${file_name%%.*}_$(date "+%Y%m%d%H%M%S").tar.gz"
    tar -czf "$archive_dir/$archive_name" -C "$(dirname "$latest_file")" "$file_name"
    mv "$archive_dir/$archive_name" "$archive_dir"
    rm "$latest_file"
fi