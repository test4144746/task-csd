# task-CSD

## task-1

1) нужно положить файл 1task.sh в папку /tmp/test/files
2) изменить права с "chmod 775 1task.sh"
3) заспустить "./1task.sh" если нужно в ручную заспустить! 
4) установить cron "apt-get install -y cron"
5) добавить время "cron job example */15 * * * * sh /tmp/test/files/1task.sh"


## task-2

1) установить ansible, python "apt-get install -y python3" "apt-get install -y ansible"
2) нужно изменить inventory/hosts.ini на адрес нужного сервера
3) заспустить ansible "ansible-playbook -i inventory/hosts.ini playbooks.yml -Kkb", дальше нужно прописать ssh key (попросит 2 раза. так как 1 для входа 1 для root)

## task-3 

1) в папке Docker-task3
2) docker build -t name .

## task-5

1) в папке task-5
2) docker build -t test:test
3) docker compose up -d